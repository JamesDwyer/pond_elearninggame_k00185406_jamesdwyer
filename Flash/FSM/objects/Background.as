﻿package objects
{
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	
	
	public class Background extends Sprite
	{
		private var menu_Bg:Menu_Bg;
		private var gameOver_Bg:GameOver_Bg;
		private var start_Bg:Start_Bg;
		private var settings_Bg:Settings_Bg;
		private var instructions_Bg:Instructions_Bg;
		private var leaderboards_Bg:Leaderboards_Bg;
		private var play_Bg: Play_BG;
		private var grassScene:grass_scene;
		private var winterScene: Winter_Scene;
		private var desertScene: DesertScene;
		private var mini_Game_BG: Mini_Game_BG;
		public static const MENU_BG:int = 1;
		public static const START_BG:int = 2;
		public static const GAME_OVER_BG:int = 3;
		public static const INSTRUCTIONS_BG:int = 4;
		public static const LEADERBOARDS_BG:int = 5;
		public static const SETTINGS_BG:int = 6;
		public static const PLAY_BG:int=7;
		public static const GRASS_SCENE:int = 8;
		public static const WINTER_SCENE:int=9;
		public static const DESERT_SCENE:int=10;
		public static const MINI_GAME_BG: int=11;
		
		
		
	
		public function Background(i:int)
		{
			
			
			switch(i){
                                       			
				case 1 :
					menu_Bg = new Menu_Bg();
					addChild(menu_Bg);
					break;
				case 2 :
					start_Bg = new Start_Bg();
					addChild(start_Bg);
					break;
				case 3 :
					gameOver_Bg = new GameOver_Bg();
					addChild(gameOver_Bg);
					break;
				case 4 :
					instructions_Bg = new Instructions_Bg();
					addChild(instructions_Bg);
					break;
				case 5 :
					leaderboards_Bg = new Leaderboards_Bg();
					addChild(leaderboards_Bg);
					break;
				case 6 :
					settings_Bg = new Settings_Bg();
					addChild(settings_Bg);
					break;
				case 7 :
					play_Bg = new Play_BG();
					addChild(play_Bg);
					break;
				case 8 :
					grassScene = new grass_scene();
					addChild(grassScene);
					break;
				case 9 :
					winterScene = new Winter_Scene();
					addChild(winterScene);
					break;
				case 10 :
					desertScene = new DesertScene();
					addChild(desertScene);
					break;
				case 11 :
					mini_Game_BG = new Mini_Game_BG();
					addChild(mini_Game_BG);
					break;
				case 0:
					menu_Bg = new Menu_Bg();
					addChild(menu_Bg);
					break;
			
			}			
		}
		
		public function update():void
		{
		
		}
		public function changeState(state:int):void
		{
			
			
			switch(state)
			{
				case MENU_BG:
					Background(1);
					break;
				
				case START_BG:
					Background(2);
					break;
				
				case GAME_OVER_BG:
					Background(3);
					break;
				
				case INSTRUCTIONS_BG:
					Background(4);
					break;
				case LEADERBOARDS_BG:
					Background(5);
					break;
				case SETTINGS_BG:
					Background(6);
					break;
				case PLAY_BG:
					Background(7);
					break;
				case GRASS_SCENE:
					Background(8);
					break;
				case WINTER_SCENE:
					Background(9);
					break;
				case DESERT_SCENE:
					Background(10);
					break;
				case MINI_GAME_BG:
					Background(11);
					break;
			}
			
		
		}
	public function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}