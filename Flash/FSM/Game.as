﻿package 
{
	import interfaces.IState;
	
	import flash.display.Sprite;
	import flash.events.Event;
	
	import states.Menu;
	import states.Play;
	import states.Instructions;
	import states.GameOver;
	import states.Settings;
	import states.Leaderboards;
	import states.Start;
	import states.Mini_Game;
	
	public class Game extends Sprite
	{
		public static const MENU_STATE:int = 0;
		public static const PLAY_STATE:int = 1;
		public static const INSTRUCTIONS_STATE:int = 2;
		public static const GAME_OVER_STATE:int = 3;
		public static const SETTINGS_STATE:int = 4;
		public static const LEADERBOARDS_STATE:int = 5;
		public static const START_STATE:int = 6;
		public static const MINI_GAME_STATE:int = 7;
		
		private var current_state:IState;
		
		public function Game()
		{
	
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			changeState(START_STATE);
			addEventListener(Event.ENTER_FRAME, update);
		}
		
		public function changeState(state:int):void
		{
			if(current_state != null)
			{
				current_state.destroy();
				current_state = null;
			}
			
			switch(state)
			{
				case MENU_STATE:
					current_state = new Menu(this);
					break;
				
				case PLAY_STATE:
					current_state = new Play(this);
					break;
				
				case INSTRUCTIONS_STATE:
					current_state = new Instructions(this);
					break;
				
				case GAME_OVER_STATE:
					current_state = new GameOver(this);
					break;
				case LEADERBOARDS_STATE:
					current_state = new Leaderboards(this);
					break;
				case SETTINGS_STATE:
					current_state = new Settings(this);
					break;
				case START_STATE:
					current_state = new Start(this);
					break;
				case MINI_GAME_STATE:
					current_state = new Mini_Game(this);
			}
			
			addChild(Sprite(current_state));
		}
		
		private function update(event:Event):void
		{
			current_state.update();
		}
	}
}