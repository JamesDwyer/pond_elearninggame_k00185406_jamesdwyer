﻿package states
{
	import Game;
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.MouseEvent;
	import fl.controls.Button;
	import objects.Background;
	import objects.Title;

	import interfaces.IState;
	
	public class Instructions extends Sprite implements IState
	{
		public var game:Game;
		private var background:Background;
		private var back_Btn: Back_Btn;
		
		public function Instructions(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			background = new Background(Background.INSTRUCTIONS_BG);
			background.y=-46;
			addChild(background);
			
			back_Btn = new Back_Btn();
			back_Btn.x=50;
			back_Btn.y=600;
			addChild(back_Btn);
			
			back_Btn.addEventListener(MouseEvent.CLICK, onClick);
					
		
		}

		private function onClick(event:Event):void
		{
			game.changeState(Game.MENU_STATE);
		}
		

		
		public function update():void
		{
			background.update();
			background = null;
		}
		
		public function destroy():void
		{
			removeFromParent();
		}
		
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}