﻿package states
{
	import Game;
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import fl.controls.Button;
	import objects.Background;	
	import objects.Title;
	import interfaces.IState;
	import fl.motion.easing.Back;
	
	public class Leaderboards extends Sprite implements IState
	{
		public var game:Game;
		private var background:Background;
		private var back_Btn: Back_Btn;
		
		
		public function Leaderboards(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			background = new Background(Background.LEADERBOARDS_BG);
			background.y=-37;
			addChild(background);
			
			back_Btn = new Back_Btn();
			back_Btn.x=50;
			back_Btn.y=600;
			addChild(back_Btn);
			
			back_Btn.addEventListener(MouseEvent.CLICK, onClick);
					
		
		}

		private function onClick(event:Event):void
		{
			game.changeState(Game.MENU_STATE);
		}
		public function update():void
		{
			
			
		}
		
		public function destroy():void
		{
			removeFromParent();
			background = null;
		}
		
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}