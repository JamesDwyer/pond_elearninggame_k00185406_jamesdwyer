﻿package states
{
	import Game;
	
	import interfaces.IState;
	
	import objects.Background;
	import objects.Title;
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import fl.controls.Button;
	import flash.text.TextField;
	import states.Play;
	import flash.text.TextFormat;
	
	public class GameOver extends Sprite implements IState
	{
		private var game:Game;
		private var background:Background;
		private var menu_Btn: Menu_Btn;
		private var reload_Btn:Reload_Btn;
		private var score_Tf:TextField;
	
		
		public function GameOver(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			background = new Background(Background.GAME_OVER_BG);
			addChild(background);
			
			menu_Btn= new Menu_Btn();
			menu_Btn.x=400;
			menu_Btn.y=615;
			addChild(menu_Btn);
			
			reload_Btn = new Reload_Btn();
			reload_Btn.x=650;
			reload_Btn.y=605;
			addChild(reload_Btn);
			
			var myFormat:TextFormat = new TextFormat();
			myFormat.size = 50;
			score_Tf = new TextField();
			score_Tf.defaultTextFormat=myFormat;
			score_Tf.textColor = 0x62454e;
			score_Tf.text = Play.score.toString();
			score_Tf.x=650;
			score_Tf.y=355;
			addChild(score_Tf);
			menu_Btn.addEventListener(MouseEvent.CLICK ,onMenu);
			reload_Btn.addEventListener(MouseEvent.CLICK ,onPlay);
			
			

			
		}

		private function onPlay(event:Event):void
		{
			game.changeState(Game.PLAY_STATE);
		}
		
		private function onMenu(event:Event):void
		{
			game.changeState(Game.MENU_STATE);
		}
			
		
		private function onAgain(event:Event):void
		{
		
			game.changeState(Game.PLAY_STATE);
		}
		
		public function update():void
		{
			background.update();
		}
		
		public function destroy():void
		{
			removeFromParent();
			background = null;
		}
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}