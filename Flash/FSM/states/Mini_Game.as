﻿package states
{
	import Game;
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import fl.controls.Button;
	import objects.Background;	
	import objects.Title;
	import interfaces.IState;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	public class Mini_Game extends Sprite implements IState
	{
		public var game:Game;
		public static var score:int=0;
		private var background:Background;
		private var frog:Frog;
		private var button1 : Jump;
		private var button2 : Double_Jump;
		private var jumpCount :int;
		private var sceneCount:int =1;
		private var lilyPad_Array : Array;
		
		
		
		public function Mini_Game(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			background = new Background(Background.GRASS_SCENE);
		
			addChild(background);
			lilyPad_Array = new Array();
			generateLilypad();
			
			frog = new Frog();
			frog.x=40;
			frog.y=455;
			addChild(frog);
			
			button1 = new Jump();
			button2 = new Double_Jump();
			button1.x = 100;
			button1.y= 600;
			button2.x = 475;
			button2.y= 600;
			addChild(button1);
			addChild(button2);
			
			
			
		
			
			button1.addEventListener(MouseEvent.CLICK, onButton1);
			button2.addEventListener(MouseEvent.CLICK, onButton2);
			addEventListener(Event.ENTER_FRAME, checkCount);
			

			
			
			
			
		}
		
		private function onGameOver():void
		{
			
			game.changeState(Game.GAME_OVER_STATE);
			
		}
		private function generateLilypad(){
			var lilypad_Temp : Lily_Pad;
			for (var i:int=0; i<8;i++){
				 lilypad_Temp = new Lily_Pad();
				lilypad_Temp.x=40+(158*i);
				lilypad_Temp.y=500;
				lilyPad_Array.push(lilypad_Temp);
				addChild(lilypad_Temp);
				
				trace("lilypad created");
				if ((Math.floor(1+Math.random()*2))==2){
					lilypad_Temp.visible = false;	
					}
				
			}
		}
		
		private function onButton1(event:Event):void
		{
			frog.x+=158
			jumpCount++;
		}
		/*private function moveBG(event:Event):void
		{
			if(background.x>-2560){
				
		
			background.x-=10;
		}
			}*/
		
		private function onButton2(event:Event):void
		{
			frog.x = frog.x +316;
			jumpCount+=2;
		}
		private function checkCount(event:Event):void
		{
			if(jumpCount>=8 && sceneCount==1){
				jumpCount=0;
				sceneCount++;
				frog.x=40;
				removeChild(background);
				removeChild(frog);
				removeChild(button1);
				removeChild(button2);
				button1.removeEventListener(MouseEvent.CLICK, onButton1);
				button2.removeEventListener(MouseEvent.CLICK, onButton2);
				background= new Background(Background.DESERT_SCENE);
				addChild(background);
				addChild(frog);
				addChild(button1);
				addChild(button2);
				button1.addEventListener(MouseEvent.CLICK, onButton1);
				button2.addEventListener(MouseEvent.CLICK, onButton2);
			}
			
			if(jumpCount>=8&& sceneCount==2){
				jumpCount=0;
				sceneCount++;
				frog.x=40;
				removeChild(background);
				removeChild(frog);
				removeChild(button1);
				removeChild(button2);
				button1.removeEventListener(MouseEvent.CLICK, onButton1);
				button2.removeEventListener(MouseEvent.CLICK, onButton2);
				background= new Background(Background.WINTER_SCENE);
				addChild(background);
				addChild(frog);
				addChild(button1);
				addChild(button2);
				button1.addEventListener(MouseEvent.CLICK, onButton1);
				button2.addEventListener(MouseEvent.CLICK, onButton2);
			}
			if(jumpCount>=8&& sceneCount==3){
				game.changeState(Game.GAME_OVER_STATE);
			}
		}
		

		
		private function onMenu(event:Event):void
		{
			game.changeState(Game.MENU_STATE);
		}
		public function update():void
		{
			background.update();
			
		}
		
		public function destroy():void
		{
			removeFromParent();
			removeChild(background);
			removeChild(frog);
			removeChild(button1)
			removeChild(button2)
			button1.removeEventListener(MouseEvent.CLICK, onButton1);
			button2.removeEventListener(MouseEvent.CLICK, onButton2);
			removeEventListener(Event.ENTER_FRAME, checkCount);
			
			
		}
		
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}