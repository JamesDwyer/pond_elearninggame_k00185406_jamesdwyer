﻿package states
{
	import Game;
	
	import interfaces.IState;
	
	import objects.Background;
	import objects.Title;
	
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import fl.controls.Button;
	
	
	public class Menu extends Sprite implements IState
	{
		private var game:Game;
		private var background:Background;
		private var play_Btn: Play_Btn;
		private var leaderboards_Btn :Leaderboards_Btn;
		private var instructions_Btn:Instructions_Btn;
		private var settings_Btn:Settings_Btn;
		private var back_Btn : Back_Btn;
		
		
		public function Menu(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			background = new Background(Background.MENU_BG);
			background.y-=55;
			addChild(background);
			
			play_Btn = new Play_Btn();
			instructions_Btn = new Instructions_Btn();
			leaderboards_Btn = new Leaderboards_Btn();
			settings_Btn = new Settings_Btn();
			
			play_Btn.x =500;
			play_Btn.y=300;
			instructions_Btn.x=500;
			instructions_Btn.y=400;
			leaderboards_Btn.x=500;
			leaderboards_Btn.y=500;
			settings_Btn.x=500;
			settings_Btn.y=600;
			
			back_Btn = new Back_Btn();
			back_Btn.x=50;
			back_Btn.y=300;
			addChild(back_Btn);

			addChild(play_Btn);
			addChild(instructions_Btn);
			addChild(leaderboards_Btn);
			addChild(settings_Btn);
			
			
			play_Btn.addEventListener(MouseEvent.CLICK,onPlay)
			instructions_Btn.addEventListener(MouseEvent.CLICK,onInstructions)
			leaderboards_Btn.addEventListener(MouseEvent.CLICK,onLeaderboards)
			settings_Btn.addEventListener(MouseEvent.CLICK,onSettings)
			back_Btn.addEventListener(MouseEvent.CLICK,onBack);
		}
		
		private function onPlay(event:Event):void
		{
			game.changeState(Game.PLAY_STATE);
		}
		
		private function onBack(event:Event):void
		{
			game.changeState(Game.MINI_GAME_STATE);
		}
		private function onInstructions(event:Event):void
		{
			game.changeState(Game.INSTRUCTIONS_STATE);
		}
		private function onLeaderboards(event:Event):void
		{
			game.changeState(Game.LEADERBOARDS_STATE);
		}
		private function onSettings(event:Event):void
		{
			game.changeState(Game.SETTINGS_STATE);
		}
		
		public function update():void
		{
			background.update();
		}
		
		public function destroy():void
		{
			background.removeFromParent();
			background = null;
			
			removeFromParent();
		}
		
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}