﻿package states
{
	import Game;
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import fl.controls.Button;
	import objects.Background;	
	import objects.Title;
	import interfaces.IState;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	public class Start extends Sprite implements IState
	{
		public var game:Game;
		private var background:Background;
		
	
		public function Start(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			background = new Background(Background.START_BG);
			addChild(background);
			var myTimer:Timer = new Timer(2000,1);
			myTimer.addEventListener(TimerEvent.TIMER, timerListener);
			myTimer.start();
		
		}
		
		public function timerListener (e:TimerEvent):void{
			this.game.changeState(0);
		}
		
		public function update():void{
			
			
		}
		public function destroy():void
		{
			removeFromParent();
			background = null;
		}
		
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}