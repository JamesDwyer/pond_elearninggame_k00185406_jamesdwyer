﻿package states
{
	import Game;
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import fl.controls.Button;
	import objects.Background;	
	import objects.Title;
	import interfaces.IState;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	public class Play extends Sprite implements IState
	{
		public var game:Game;
		public static var score:int=0;
		private var background:Background;
		private var frog:Frog;
		private var num1:int;
		private var num2:int;
		private var answer1:int;
		private var answer2:int;
		private var answer3:int;
		private var correctAnswer:int;
		private var isCorrect:Boolean;
		private var button1 : Button;
		private var button2 : Button;
		private var button3 : Button;
		private var text1:TextField;
		private var text2:TextField;
		private var text3:TextField;
		private var q1:TextField;
		private var q2:TextField;
		private var plus:TextField;
		private var equals:TextField;
		private var lives:int=3;
		private var numCorrect:int=0;
		private var numx : int;
		
		
		
		public function Play(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			background = new Background(Background.PLAY_BG);
		
			addChild(background);
			
			var myFormat:TextFormat = new TextFormat();
			myFormat.size = 50;
			
			frog = new Frog();
			frog.x=40;
			frog.y=455;
			addChild(frog);
			
			button1 = new Button();
			button2 = new Button();
			button3 = new Button();
			/*button1.ans.text = "99";*/
			button1.x = 100;
			button1.y= 600;
			button2.x = 475;
			button2.y= 600;
			button3.x = 850;
			button3.y= 600;
			addChild(button1);
			addChild(button2);
			addChild(button3);
			text1 = new TextField();
			text2 = new TextField();
			text3 = new TextField();
			q1 = new TextField();
			q2 = new TextField();
			plus = new TextField();
			equals = new TextField();
			plus.textColor=0xFFFFFF
			equals.textColor=0xFFFFFF;
			q1.textColor=0xFFFFFF
			q2.textColor=0xFFFFFF;
			plus.defaultTextFormat = myFormat;
			equals.defaultTextFormat = myFormat;
			q1.defaultTextFormat = myFormat;
			q2.defaultTextFormat = myFormat;
			plus.text="+";
			equals.text="="
			plus.x=900;
			plus.y=150;
			q1.text="";
			q2.text=""
			q1.x=835;
			q1.y=150;
			q2.x=935;
			q2.y=150;
			equals.x=1000;
			equals.y=145;
			addChild(plus);
			addChild(equals);
			addChild(q1);
			addChild(q2);
			
			button1.addEventListener(MouseEvent.CLICK, onButton1);
			button2.addEventListener(MouseEvent.CLICK, onButton2);
			button3.addEventListener(MouseEvent.CLICK, onButton3);
			addEventListener(Event.ENTER_FRAME, checkStatus);
			generateQuestion();
			
			
			
			
		}
		private function generateType():int{
			return (1+Math.random()*2);
		}
		private function generateQuestion():void{
			switch(generateType()){
				case 1:
				num1 = 1 + Math.random()*50;
			q1.text = num1.toString();
			num2 = 1 + Math.random()*50;
			q2.text = num2.toString();
			correctAnswer = num1+ num2
			plus.text="+";	
			numx =(1+Math.random()*3);
			switch(numx){
			case 1:
			answer1 = correctAnswer;
			answer2 = -100 + Math.random()*200;
			answer3 = 50 + Math.random()*100;
			break;
			case 2:
			answer1 = 1 + Math.random()*100;
			answer2 = correctAnswer;
			answer3 = 50 + Math.random()*100;
			break;
			case 3:
			answer1 = 50 + Math.random()*100;
			answer2 = 1 + Math.random()*100;
			answer3 = correctAnswer
			break;
			}
			text1.text=answer2.toString();
			text2.text=answer1.toString();
			text3.text=answer3.toString();
			text1.x=250;
			text1.y=630;
			text2.x=625;
			text2.y=630;
			text3.x=1000;
			text3.y=630;
			text1.selectable =false;
			text2.selectable =false;
			text3.selectable =false;
			addChild(text1);
			addChild(text2);
			addChild(text3);
				
				break;
				case 2:
				num1 = 1 + Math.random()*50;
			q1.text = num1.toString();
			num2 = 1 + Math.random()*50;
			q2.text = num2.toString();
			correctAnswer = num1 - num2;
			plus.text ="-";
			 numx =(1+Math.random()*3);
			switch(numx){
			case 1:
			answer1 = correctAnswer;
			answer2 = -100 + Math.random()*200;
			answer3 = 50 + Math.random()*100;
			break;
			case 2:
			answer1 = 1 + Math.random()*100;
			answer2 = correctAnswer;
			answer3 = 50 + Math.random()*100;
			break;
			case 3:
			answer1 = 50 + Math.random()*100;
			answer2 = 1 + Math.random()*100;
			answer3 = correctAnswer
			break;
			}
			text1.text=answer2.toString();
			text2.text=answer1.toString();
			text3.text=answer3.toString();
			text1.x=250;
			text1.y=630;
			text2.x=625;
			text2.y=630;
			text3.x=1000;
			text3.y=630;
			text1.selectable =false;
			text2.selectable =false;
			text3.selectable =false;
			addChild(text1);
			addChild(text2);
			addChild(text3);
				
			
		}	
			
			
		}
		private function onGameOver():void
		{
			
			game.changeState(Game.GAME_OVER_STATE);
			
		}
		private function checkStatus(event:Event):void{
			if (lives==0){
				onGameOver();
			}
			if(numCorrect==8){
				game.changeState(Game.MINI_GAME_STATE);
			}
		}
		private function onCorrect():void{
			frog.x+=158;
			generateQuestion();
			numCorrect++;
			score = numCorrect*100;
		}
		private function onButton1(event:Event):void
		{
			if (text1.text == correctAnswer.toString()){
				onCorrect();
			}
			else{
				generateQuestion();
				lives--;
			}
		}
		
		private function onButton2(event:Event):void
		{
			if (text2.text == correctAnswer.toString()){
				onCorrect();
			}
			else{
				generateQuestion();
				lives--;
			}
		}
		
		private function onButton3(event:Event):void
		{
			if (text3.text == correctAnswer.toString()){
				onCorrect();
			}
			else{
				generateQuestion();
				lives--;
			}
		}
		
		
		private function onMenu(event:Event):void
		{
			game.changeState(Game.MENU_STATE);
		}
		public function update():void
		{
			background.update();
			
		}
		
		public function destroy():void
		{
			removeFromParent();
			removeChild(background);
			removeChild(frog);
			removeChild(button1)
			removeChild(button2)
			removeChild(button3)
			removeChild(q1);
			removeChild(q2);
			removeChild(plus);
			removeChild(equals);
			button1.removeEventListener(MouseEvent.CLICK, onButton1);
			button2.removeEventListener(MouseEvent.CLICK, onButton2);
			button3.removeEventListener(MouseEvent.CLICK, onButton3);
			removeEventListener(Event.ENTER_FRAME,checkStatus);
			
		}
		
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}